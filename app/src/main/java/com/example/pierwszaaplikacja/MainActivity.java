package com.example.pierwszaaplikacja;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int delta,a,b,c,x1,x2,x0;

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public int getDelta() {
        return delta;
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public int getX0() {
        return x0;
    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public void setX0(int x0) {
        this.x0 = x0;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void funkcjaKwadratowa(View view) {
        EditText a=(EditText) findViewById(R.id.editText);
        EditText b=(EditText) findViewById(R.id.editText2);
        EditText c=(EditText) findViewById(R.id.editText3);
        TextView wynik=(TextView) findViewById(R.id.textView);
        setA(Integer.parseInt(a.getText().toString()));
        setB(Integer.parseInt(b.getText().toString()));
        setC(Integer.parseInt(c.getText().toString()));
        setDelta(getB()*getB()-4*getA()*getC());

        if(getDelta()<0)
        {
            wynik.setText("Brak rozwiazan rzeczywistych");
        }
        else if(getDelta()==0)
        {
            setX0(-getB()/(2*getA()));
            wynik.setText(String.valueOf(getX0()));
        }
        else
        {
            wynik.setText("tu beda dwa pierwiastki");
        }


    }
}
